# particlenet-job

Run ParticleNet as a PyTorch Job on Kubeflow

## Setup

- Open a server on https://ml.cern.ch/_/jupyter
    - Custom image: `gitlab-registry.cern.ch/dholmber/particlenet-images/notebook:latest`
- Opem a terminal in the notebook server and clone this repo
    - `git clone https://gitlab.cern.ch/dholmber/particlenet-job.git; cd particlenet-job`
- Authenticate with kerberos
    - `kinit <cernid>`
- When kerberos has been refreshed, remove any old secret before creating a new one
    - `kubectl delete secret krb-secret`
- Create a kerberos secret for Kubernetes
    - `kubectl create secret generic krb-secret --from-file=/tmp/krb5cc_1000`

## Run

- Modify a jobfile
    - `pytorch.yaml` = single node training
    - `multigpu.yaml` = multi node training
    - `hp-tuning.yaml` = hyperparameter tuning
- Run a job
    - `kubectl apply -f pytorch.yaml`

## Monitor

- List pods
    - `kubectl get pods`
- Describe the job
    - `kubectl describe pod pytorch-job-master-0`
- Display job output
    - `kubectl logs pytorch-job-master-0`

## Terminate

- Gracefully
    - `kubectl delete -f pytorch.yaml`
- With force
    - `kubectl delete pod pytorch-job-master-0 --force --grace-period 0`

## HP tuning

- Change the `namespace` in `hp-tuning.yaml` to your name
    - Namespace appears if you go to https://ml.cern.ch/?ns= after `?ns=` in the link
- Submit via the terminal as usual
    - `kubectl apply -f hp-tuning.yaml`
- _Or_ paste the yaml in the UI https://ml.cern.ch/katib/#/katib/hp
- You should see that an experiment has been created
    - `kubectl get experiments`
- Monitoring and debugging can be done using the commands for pods
- Visualizations of successful runs are found on Katib https://ml.cern.ch/_/katib/#/katib/hp_monitor

![](hp-tuning.png)

## Additional documentation
- https://ml.docs.cern.ch
- https://www.kubeflow.org/docs/components/training/pytorch
